# CoolTool

A sample python project structure with namespaces.

## Installation

Clone the repo, `cd` into it, and run:


```bash
pip install -e .[dev]
```

This will pull the basic packages, and make the project ready for development.