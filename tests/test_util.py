from pyfoons.cooltool.util import reverse_str

def test_util_reverse_str():
    s = 'foo'
    assert s[::-1] == reverse_str(s)