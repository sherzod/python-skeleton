from setuptools import setup, find_namespace_packages

# or read from a markdown file, your call
long_description = """#Why

It all began with a bang, and when it was over, I decided to write
this tool to understand it all. You could tell you, but I can also show you.
"""

setup(
    name="cool-tool",
    version="0.0.1",
    url="https://get.awebsite.com",
    description="Short, and brief description, that's not too long...",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="John Doe",
    author_email="john.doe@ffs.com",
    package_dir={'': 'src'},
    packages=find_namespace_packages(where='src', include="pyfoons.*"),
    install_requires=[
        'Click'
    ],
    extras_require={
        'dev': ['pytest']
    },
    entry_points={
        'console_scripts': [
            'foocli = pyfoons.cooltool.cli.app:cli'
        ]
    }

)